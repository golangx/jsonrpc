package jsonrpc

const (
	// ParseError Parse error
	// Invalid JSON was received by the server.
	ParseError = -32600
)

// ParseError JSON解析错误
// func ParseError() RPCError {
// 	return RPCError{
// 		Code:    -32600,
// 		Message: "Parse error",
// 		Data:    "Invalid JSON was received by the server.",
// 	}
// }
/**
-------------------------------------------
-----------------------------------------
*/

// // ParseError JSON解析错误
// const ParseError error = &RPCError{
// 	Code:    -32600,
// 	Message: "Parse error",
// 	Data:    "Invalid JSON was received by the server.",
// }

// InvalidRequest JSON解析错误
func InvalidRequest() RPCError {
	return RPCError{
		Code:    -32600,
		Message: "Invalid Request",
		Data:    "The JSON sent is not a valid Request object.",
	}
}

// MethodNotFound JSON解析错误
func MethodNotFound() RPCError {
	return RPCError{
		Code:    -32600,
		Message: "Method not found",
		Data:    "The method does not exist / is not available.",
	}
}

// InvalidParams JSON解析错误
func InvalidParams() RPCError {
	return RPCError{
		Code:    -32600,
		Message: "Invalid params",
		Data:    "Invalid method parameter(s).",
	}
}

// InternalError JSON解析错误
func InternalError() RPCError {
	return RPCError{
		Code:    -32603,
		Message: "Internal error",
		Data:    "Internal JSON-RPC error.",
	}
}

// ServerError JSON解析错误
func ServerError() RPCError {
	return RPCError{
		Code:    -32000,
		Message: "Server error",
		Data:    "Reserved for implementation-defined server-errors.",
	}
}
